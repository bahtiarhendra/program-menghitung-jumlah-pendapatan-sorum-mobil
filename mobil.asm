.model small
.code
org 100h
start :
	jmp mulai
	nama	db,13,10, 'Nama Anda	:$'
	hp	db 13,10, 'No HP/Telp	:$'
	kode	db 13,10,'Kode/no mobil yang anda pilih	:$'
	lanjut	db 13,10,'LANJUT Tekan x untuk lanjut >>>>>>>>>>>>>>>>>>$'
	tampung_nama	db 30,?,30 dup(?)
	tampung_hp	db 13,?, 13 dup(?)
	tampung_kode	db 13,?, 13 dup(?)

a db 01
b db 02
c db 03
d db 04
e db 05
f db 06
g db 07
h db 08
i db 09
j dw 15

daftar db 13,10,'+===+===========+=======+=======+========================'
	db 13,10,'		 DAFTAR HARGA MOBIL 			 '
	db 13,10,'		 CV SEJAHTERA ABADI			 '
	db 13,10,'+==+===========+=======+=======+========================'
	db 13,10,'|No|Type		|Warna	|Tahun  |Harga		|'
	db 13,10,'|01|New Corolla Altis 	|Putih	|2021   |Rp. 470.500.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|02|FORTUNER MT DIESEL	|Hitam	|2021	|Rp. 533.460.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|03|FORTUNER AT DIESEL	|Abu-Abu|2021	|Rp. 508.250.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|04|INNOVA REBORN A MT	|Hitam	|2020	|Rp. 324.700.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|05|INNOVA G MT	|Abu-Abu|2016	|Rp. 180.200.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+','$'


daftar2 db 13,10,'+==+===================+=======+=======+================'
	db 13,10,'|No|Type		|Warna	|Tahun  |Harga  	|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|06|JAZZ RS		|Putih	|2019	|Rp. 215.400.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|07|Yaris GR Sport	|Merah 	|2021	|Rp. 259.900.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|08|RUSH G AT		|Hitam	|2019	|Rp. 212.650.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|09|MOBILIO RS MT	|Hitam	|2017	|Rp. 161.000.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|10|AVANZA G AT	|Abu-Abu|2016	|Rp. 115.000.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+','$'

daftar3 db 13,10,'+==+===================+=======+=======+================'
	db 13,10,'|No|Type		|Warna	|Tahun  |Harga  	|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|11|AYLA 1,2 R  MT	|Metalic|2015	|112.500.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|12|SIGRA 1,0 D AT	|Merah	|2016	|110.100.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|13|GRANMX 1.3 D AT	|Putih	|2018	|140.400.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|14|TERIOS 1,5 MT	|Putih	|2019	|220.000.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,30,'|15|GRANMAX PU 1,3 STD	|Hitam	|2017	|108.000.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+','$'


daftar4 db 13,10,'+==+===================+=======+=======+================'
	db 13,10,'|No|Type		|Warna	|Tahun  |Harga  	|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|16|L300 PU		|Hitam	|2017	|Rp. 120.500.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|17|CIVIC 		|Hitam	|2002	|Rp. 86.000.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|18|BRIO SATYA	|Kuning	|2020	|Rp. 184.300.000|'	
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|19|L300 BOX		|Hitam	|2016	|Rp. 118.000.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|20|AGYA 1,0 G		|Putih	|2017	|Rp. 104.650.000|'
	db 13,10,'+--+-------------------+-------+-------+---------------+'
	db 13,10,'|TENTUKAN MOBIL PILIHAN ANDA SESUAI DENGAN KODE PADA TABEL|'
	db 13,10,'+------------------------------------------------------+','$'


error		db 13,10,'KODE YANG ANDA MASUKAN SALAH $'
pilih_mtr	db 13,10,'Silahkan masukan kode mobil yang anda pilih = $'
success		db 13,10,'Selamat Anda Berhasil $'
	
	mulai :
	mov ah,09h
	lea dx,nama
	int 21h
	mov ah,0ah
	lea dx,tampung_nama
	int 21h
	push dx

	mov ah,09h
	lea dx,hp
	int 21h
	mov ah,0ah
	lea dx,tampung_hp
	int 21h
	push dx

	mov ah,09h
	mov dx,offset daftar
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'x'
	je page2
	jne error_msg

page2 :
	mov ah,09h
	mov dx,offset daftar2
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'x'
	je page3
	jne error_msg

page3 :
	mov ah,09h
	mov dx,offset daftar3
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'x'
	je page4
	jne error_msg

page4 :
	mov ah,09h
	mov dx,offset daftar4
	int 21h
	mov ah,09h
	mov dx,offset lanjut
	int 21h
	mov ah,01h
	int 21h
	cmp al,'x'

mov ah,09h
	lea dx,kode
	int 21h
	mov ah,01h
	int 21h
	cmp al,'x'
	jmp proses
	jne error_msg

error_msg :
	mov ah,09h
	mov dx,offset error
	int 21h
	int 20h

proses :
	mov ah,09h
	mov dx,offset pilih_mtr
	int 21h

	mov ah,1
	int 21h
	mov bh,al
	mov ah,1
	int 21h
	mov bl,al
	cmp bh,'0'
	cmp bl,'1'
	je hasil1

	cmp bh,'0'
	cmp bl,'2'
	je hasil2

	cmp bh,'0'
	cmp bl,'3'
	je hasil3

	cmp bh,'0'
	cmp bl,'4'
	je hasil4

	cmp bh,'0'
	cmp bl,'5'
	je hasil5

	cmp bh,'0'
	cmp bl,'6'
	je hasil6

	cmp bh,'0'
	cmp bl,'7'
	je hasil7

	cmp bh,'0'
	cmp bl,'8'
	je hasil8

	cmp bh,'0'
	cmp bl,'9'
	je hasil9

	;cmp bh,'1'
	;cmp bl,'0'
	;je hasil10

	;cmp bh,'1'
	;cmp bl,'1'
	;je hasil11

	;cmp bh,'1'
	;cmp bl,'2'
	;je hasil12

	;cmp bh,'1'
	;cmp bl,'3'
	;je hasil13

	;cmp bh,'1'
	;cmp bl,'4'
	;je hasil14

	;cmp bh,'1'
	;cmp bl,'5'
	;je hasil15

	;cmp bh,'1'
	;cmp bl,'6'
	;je hasil16

	;cmp bh,'1'
	;cmp bl,'7'
	;je hasil17

	;cmp bh,'1'
	;cmp bl,'8'
	;je hasil18

	;cmp bh,'1'
	;cmp bl,'9'
	;je hasil19

	;cmp bh,'2'
	;cmp bl,'0'
	;je hasil20

	;cmp bh,'2'
	;cmp bl,'1'
	;je hasil21
	
	;cmp bh,'2'
	;cmp bl,'2'
	;je hasil22

	;cmp bh,'2'
	;cmp bl,'3'
	;je hasil23

	;cmp bh,'2'
	;cmp bl,'4'
	;je hasil24

	jne error_msg

;=======================================

hasil1:
	mov ah,09h
	lea dx,teks1
	int 21h
	int 20h

hasil2:
	mov ah,09h
	lea dx,teks2
	int 21h
	int 20h

hasil3:
	mov ah,09h
	lea dx,teks3
	int 21h
	int 20h

hasil4:
	mov ah,09h
	lea dx,teks4
	int 21h
	int 20h

hasil5:
	mov ah,09h
	lea dx,teks5
	int 21h
	int 20h

hasil6:
	mov ah,09h
	lea dx,teks6
	int 21h
	int 20h

hasil7:
	mov ah,09h
	lea dx,teks7
	int 21h
	int 20h

hasil8:
	mov ah,09h
	lea dx,teks8
	int 21h
	int 20h

hasil9:
	mov ah,09h
	lea dx,teks9
	int 21h
	int 20h

hasil10:
	mov ah,09h
	lea dx,teks10
	int 21h
	int 20h

hasil11:
	mov ah,09h
	lea dx,teks11
	int 21h
	int 20h

hasil12:
	mov ah,09h
	lea dx,teks12
	int 21h
	int 20h

hasil13:
	mov ah,09h
	lea dx,teks13
	int 21h
	int 20h

hasil14:
	mov ah,09h
	lea dx,teks14
	int 21h
	int 20h

hasil15:
	mov ah,09h
	lea dx,teks15
	int 21h
	int 20h

hasil16:
	mov ah,09h
	lea dx,teks16
	int 21h
	int 20h

hasil17:
	mov ah,09h
	lea dx,teks17
	int 21h
	int 20h

hasil18:
	mov ah,09h
	lea dx,teks18
	int 21h
	int 20h

hasil19:
	mov ah,09h
	lea dx,teks19
	int 21h
	int 20h

hasil20:
	mov ah,09h
	lea dx,teks20
	int 21h
	int 20h

;==========================================

teks1	db 13,10,'Anda Telah Memilih Mobil NEW COROLLA ALTIS'
	db 13,10,'Warna Putih'
	db 13,10,'Keluaran Tahun 2021'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 470.500.000'
	db 13,10,'Terima Kasih $'

teks2	db 13,10,'Anda Telah Memilih Mobil FORTUNER MT DIESEL'
	db 13,10,'Warna Hitam'
	db 13,10,'Keluaran Tahun 2021'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 533.460.000'
	db 13,10,'Terima Kasih $'

teks3	db 13,10,'Anda Telah Memilih Mobil FORTUNER AT DIESEL'
	db 13,10,'Warna Abu-Abu'
	db 13,10,'Keluaran Tahun 2021'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 508.250.000'
	db 13,10,'Terima Kasih $'

teks4	db 13,10,'Anda Telah Memilih Mobil INNOVA REBORN A MT'
	db 13,10,'Warna Hitam'
	db 13,10,'Keluaran Tahun 2020'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 324.700.000'
	db 13,10,'Terima Kasih $'

teks5	db 13,10,'Anda Telah Memilih Mobil INNOVA G MT'
	db 13,10,'Warna Abu-Abu'
	db 13,10,'Keluaran Tahun 2017'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 180.200.000'
	db 13,10,'Terima Kasih $'

teks6	db 13,10,'Anda Telah Memilih Mobil JAZZ RS'
	db 13,10,'Warna Putih'
	db 13,10,'Keluaran Tahun 2019'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 215.400.000'
	db 13,10,'Terima Kasih $'

teks7	db 13,10,'Anda Telah Memilih Mobil YARIS GR SPORT'
	db 13,10,'Warna Merah'
	db 13,10,'Keluaran Tahun 2021'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 259.900.000'
	db 13,10,'Terima Kasih $'

teks8	db 13,10,'Anda Telah Memilih Mobil RUSH G AT'
	db 13,10,'Warna Hitam'
	db 13,10,'Keluaran Tahun 2019'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 212.650.000'
	db 13,10,'Terima Kasih $'

teks9	db 13,10,'Anda Telah Memilih Mobil Mobilio RS MT'
	db 13,10,'Warna Hitam'
	db 13,10,'Keluaran Tahun 2017'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 161.000.000'
	db 13,10,'Terima Kasih $'


teks10	db 13,10,'Anda Telah Memilih Mobil Avanza G AT'
	db 13,10,'Warna Abu-Abu'
	db 13,10,'Keluaran Tahun 2016'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 115.000.000'
	db 13,10,'Terima Kasih $'	

teks11	db 13,10,'Anda Telah Memilih Mobil AYLA 1,2 R  MT'
	db 13,10,'Warna Metalic'
	db 13,10,'Keluaran Tahun 2015'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 112.500.000'
	db 13,10,'Terima Kasih $'	

teks12	db 13,10,'Anda Telah Memilih Mobil SIGRA 1,0 D AT'
	db 13,10,'Warna Merah'
	db 13,10,'Keluaran Tahun 2016'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 110.100.000'
	db 13,10,'Terima Kasih $'	

teks13	db 13,10,'Anda Telah Memilih Mobil GRANMX 1.3 D AT'
	db 13,10,'Warna Putih'
	db 13,10,'Keluaran Tahun 2018'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 140.400.000'
	db 13,10,'Terima Kasih $'	

teks14	db 13,10,'Anda Telah Memilih Mobil TERIOS 1,5 MT'
	db 13,10,'Warna Putih'
	db 13,10,'Keluaran Tahun 2019'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 220.000.000'
	db 13,10,'Terima Kasih $'	

teks15	db 13,10,'Anda Telah Memilih Mobil GRANMAX PU 1,3 STD'
	db 13,10,'Warna Hitam'
	db 13,10,'Keluaran Tahun 2017'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 108.000.000'
	db 13,10,'Terima Kasih $'	

teks16	db 13,10,'Anda Telah Memilih Mobil L300 PU'
	db 13,10,'Warna Hitam'
	db 13,10,'Keluaran Tahun 2017'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 120.500.000'
	db 13,10,'Terima Kasih $'	

teks17	db 13,10,'Anda Telah Memilih Mobil CIVIC'
	db 13,10,'Warna Hitam'
	db 13,10,'Keluaran Tahun 2002'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. 86.000.000'
	db 13,10,'Terima Kasih $'

teks18	db 13,10,'Anda Telah Memilih Mobil Brio Satya'
	db 13,10,'Warna Kuning'
	db 13,10,'Keluaran Tahun 2020'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. Rp. 184.300.000'
	db 13,10,'Terima Kasih $'	

teks19	db 13,10,'Anda Telah Memilih Mobil L300 BOX'
	db 13,10,'Warna Hitam'
	db 13,10,'Keluaran Tahun 2016'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. Rp. 118.000.000'
	db 13,10,'Terima Kasih $'

teks20	db 13,10,'Anda Telah Memilih Mobil AGYA 1,0 G'
	db 13,10,'Warna Putih'
	db 13,10,'Keluaran Tahun 2017'
	db 13,10,'Total Uang Yang Harus Dibayarkan = Rp. Rp. 104.650.000'
	db 13,10,'Terima Kasih $'

end start